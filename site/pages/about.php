
    <section id="" class="ftco-about d-md-flex">
    	<div class="one-half img" style="background-image: url(images/about.jpg);"></div>
    	<div class="one-half ftco-animate">
        <div class="heading-section ftco-animate ">
          <h2 class="mb-4">Bienvenue au Bar - Restaurant <span>L'Ambassade</span></h2>
        </div>
        <div>
  				<p>Venez partager de beaux moments en famille ou avec des amis. Découvrez les senteurs et les délices de nos plats Camerounais.</p>
  			</div>
    	</div>
    </section>