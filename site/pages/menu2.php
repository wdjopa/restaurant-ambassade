
    <section id="" class="ftco-menu">
    	<div class="container-fluid">
    		<div class="row d-md-flex">
	    		<div class="col-lg-4 ftco-animate img f-menu-img mb-5 mb-md-0" style="background-image: url(images/about.jpg);">
	    		</div>
	    		<div class="col-lg-8 ftco-animate p-md-5">
		    		<div class="row">
		          <div class="col-md-12 nav-link-wrap mb-5">
		            <div class="nav ftco-animate nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		              <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Sur commande</a>

		              <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Boissons</a>

		              <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">Accompagnements</a>

		              <!-- <a class="nav-link" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">Pasta</a> -->
		            </div>
		          </div>
		          <div class="col-md-12 d-flex align-items-center">
		            
		            <div class="tab-content ftco-animate" id="v-pills-tabContent">

		              <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
										<p class="text-center">
											Vous avez envie de manger quelque chose de particulier ? <br> Voici notre menu qui n'est disponible que sur <b style="color: white;">commande spéciale (5 plats minimum)</b>
										</p>
											<br>
		              	<div class="row">
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url('images/koki.jpg');"></a>
		              				<div class="text">
		              					<h3><a href="#">Koki</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€13,00/plat</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url('images/mbongo.jpg');"></a>
		              				<div class="text">
		              					<h3><a href="#">Mbongo Tchobi</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€13,00/plat</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/kondre.webp);"></a>
		              				<div class="text">
		              					<h3><a href="#">Kondrè</a>  Queue de boeuf</h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€15,00/plat</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              	</div>
		              </div>

		              <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
		                <div class="row">
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/drink-1.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Lemonade Juice</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/drink-2.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Pineapple Juice</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/drink-3.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Soda Drinks</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              	</div>
		              </div>

		              <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
		                <div class="row">
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/pommes.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Frites de pomme</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/plantain.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Frites de banane-plantain</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/attieke.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Attiéké</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              	</div>
		              </div>

		              <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
		                <div class="row">
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/pasta-1.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Itallian Pizza</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/pasta-2.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Itallian Pizza</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="col-md-4 text-center">
		              			<div class="menu-wrap">
		              				<a href="#" class="menu-img img mb-4" style="background-image: url(images/pasta-3.jpg);"></a>
		              				<div class="text">
		              					<h3><a href="#">Itallian Pizza</a></h3>
		              					<p>Les délices culinaires du Cameroun à l'Ambassade, Bar-Restaurant</p>
		              					<p class="price"><span>€2.90</span></p>
		              					<p><a href="#" class="btn btn-white btn-outline-white">Commander</a></p>
		              				</div>
		              			</div>
		              		</div>
		              	</div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
    	</div>
    </section>