
    <section id="services" class="ftco-section ftco-services">
    	<div class="overlay"></div>
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Nos Services</h2>
            <p>Une faim de soirée ? Recevez nos plats chez vous.</p>
          </div>
        </div>
    		<div class="row">
          <div class="col-md-4 ftco-animate">
            <div class="media d-block text-center block-6 services">
              <div class="icon d-flex justify-content-center align-items-center mb-5">
              	<span class="flaticon-diet"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Nourriture saine</h3>
                <p>Nos produits sont sélectionnés pour être plus sain pour vous.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-4 ftco-animate">
            <div  data-toggle="modal" data-target="#livraisonModal"  class="media d-block text-center block-6 services">
              <div class="icon d-flex justify-content-center align-items-center mb-5">
              	<span class="flaticon-bicycle"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Livraison à domicile</h3>
                <p>Nous pouvons vous livrer nos plats chez vous, passez votre commande et attendez patiemment.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="media d-block text-center block-6 services">
              <div class="icon d-flex justify-content-center align-items-center mb-5"><span class="flaticon-chef"></span></div>
              <div class="media-body">
                <h3 class="heading">Recettes Originales</h3>
                <p>Notre chef cuisinier vous fera vous sentir au Cameroun à travers son savoir-faire ancestral.</p>
              </div>
            </div>    
          </div>
        </div>
    	</div>
    </section>
