 <section class="home-slider owl-carousel img" id="accueil" style="background-image: url(images/bg_1.jpg);">
		 
      <div class="slider-item" style="background-image: url(images/bg_3.jpg);">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-7 col-sm-12 text-center ftco-animate">
            	<span class="subheading">Welcome</span>
              <h1 class="mb-4">Nous cuisinons pour vous de délicieux plats à la Camerounaise</h1>
              <p class="mb-4 mb-md-5">Retrouvez les délices de chez nous à L'Ambassade.</p>
              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Commander maintenant</a> <a href="menu.php" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Voir le menu</a></p>
            </div>

          </div>
        </div>
			</div>
			
      <div class="slider-item">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-center" data-scrollax-parent="true">

            <div class="col-md-6 col-sm-12 order-md-last ftco-animate">
            	<span class="subheading">Le Ndolè</span>
              <h1 class="mb-4">Spécialité Camerounaise</h1>
              <p class="mb-4 mb-md-5">A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Commander maintenant</a> <a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Voir le menu</a></p>
            </div>
            <div class="col-md-6 ftco-animate">
            	<img src="images/bg_2.png" class="img-fluid" style="border-radius: 30rem;" alt="">
            </div>

          </div>
        </div>
      </div>

			<div class="slider-item">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-center" data-scrollax-parent="true">

            <div class="col-md-6 col-sm-12 ftco-animate">
            	<span class="subheading">Délicieuse</span>
              <h1 class="mb-4">La cuisine Camerounaise</h1>
              <p class="mb-4 mb-md-5">Le plus aimé par nos clients, Viande de porc et frites de plantains mûrs.</p>
              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Commander maintenant</a> <a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Voir le Menu</a></p>
            </div>
            <div class="col-md-6 ftco-animate">
            	<img src="images/bg_1.png" class="img-fluid" alt="">
            </div>

          </div>
        </div>
      </div>

    </section>