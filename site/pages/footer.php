
    <footer class="ftco-footer ftco-section img">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">À propos de nous</h2>
              <p>L'ambassade est un bar restaurant souhaitant offrir aux camerounais vivant à proximité de redécouvrir l'ambiance chaleureuse de leur pays en consommant des plats délicatement cuisinés.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-md-5">
             <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Services</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Cuisson</a></li>
                <li data-toggle="modal" data-target="#livraisonModal"><a href="#!" class="py-2 d-block">Livraison</a></li>
                <li><a href="#" class="py-2 d-block">Qualité de nos produits</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Avez vous des questions ?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">32 Bis Rue d'Elbeuf, 76100 Rouen</span></li>
	                <li><a href="tel:+33782789689"><span class="icon icon-phone"></span><span class="text">+33 (0) 7 82 78 96 89</span></a></li>
	                <li><a href="mailto:info@ambassade-rouen.fr"><span class="icon icon-envelope"></span><span class="text">info@ambassade-rouen.fr</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p>
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <a href="https://ambassade-rouen.fr">Restaurant bar l'Ambassade</a> made by <a href="https://lamater.tech" target="_blank"><span style="color: goldenrod;">La Mater Tech</span></a>.</p>
          </div>
        </div>
      </div>
    </footer>


<!-- Modal -->
<div class="modal fade" id="livraisonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalScrollableTitle">Livraison</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Notre service de livraison est actuellement disponible le <span class="text-primary">Vendredi</span> et le <span class="text-primary">Samedi</span>, dans la ville de Rouen.
        <br>
        <br>
        Les frais de livraison dépendent du nombre de la taille de votre commande et de la distance. Pour en savoir plus, merci de nous appeler.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">D'accord</button>
      </div>
    </div>
  </div>
</div>