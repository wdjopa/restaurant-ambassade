
    <section id="menu" class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Notre menu classique</h2>
            <p>Les 6 plats les plus appréciés. Tous ces plats ont pour accompagnement frite de pomme, frite de banane-plantain, bâton de manioc ou attiéké</p>
          </div>
        </div>
    	</div>
    	<div class="container-wrap">
    		<div class="row no-gutters d-flex plates">
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img" style="background-image: url('images/plat (1).jpg');"></a>
    					<div class="text p-4">
    						<h3>Tilapia Braisé</h3>
    						<p>Vous aimez manger du poisson ? Vous allez adorer déguster ce plat !</p>
    						<p class="price"><span>€15,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Tilapia braisé')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img" style="background-image: url('images/plat (2).jpg');"></a>
    					<div class="text p-4">
    						<h3>Porc braisé</h3>
    						<p>Vous avez des doutes ? Ce plat est notre spécialité, recommandé par 80% de nos clients</p>
    						<p class="price"><span>€12,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Tilapia braisé')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img" style="background-image: url('images/plat (7).jpg');"></a>
    					<div class="text p-4">
    						<h3>Poulet fermier entier braisé</h3>
    						<p>Mangez votre poulet à vous ! Braisé dans les règles de l'art culinaire camerounais. Vous allez l'apprécier</p>
    						<p class="price"><span>€20,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Tilapia braisé')">Commander</a></p>
    					</div>
    				</div>
    			</div>

    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img order-lg-last" style="background-image: url('images/plat (11).jpg');"></a>
    					<div class="text p-4">
    						<h3>Eru</h3>
    						<p>Vous ne le connaissez pas ? Plat très prisé au Cameroun. Légumes cuits avec de l'huile de palme, contient des morceaux de viande. </p>
    						<p class="price"><span>€13,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Tilapia braisé')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img order-lg-last" style="background-image: url('images/plat (5).jpg');"></a>
    					<div class="text p-4">
    						<h3>Aile de poulet</h3>
    						<p>Nos ailes de poulets somptuesements cuisinés ! " C'est la magie " comme on dit au Cameroun</p>
    						<p class="price"><span>€12,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Aile de poulet')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img order-lg-last" style="background-image: url('images/plat (10).jpg');"></a>
    					<div class="text p-4">
    						<h3>Capitaine</h3>
    						<p>Poisson très populaire et très apprécié. Venez le goûter avec nos assaisonnements délicats ! </p>
    						<p class="price"><span>€20,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Capitaine')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img" style="background-image: url('images/plat (12).jpg');"></a>
    					<div class="text p-4">
    						<h3>Ndolè</h3>
    						<p>L'un des plats les plus aimé au Cameroun. Notamment par les étrangers et les natifs, essayez et vous ne serez pas déçus !</p>
    						<p class="price"><span>€13,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Ndolè')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img" style="background-image: url('images/plat (3).jpg');"></a>
    					<div class="text p-4">
    						<h3>Agneau braisé</h3>
    						<p>De la viande tendre d'agneau ?! Délicatement parfumé et cuisiné dans l'amour de nos traditions.</p>
    						<p class="price"><span>€13,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Agneau braisé')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 d-flex ftco-animate">
    				<div class="services-wrap d-flex">
    					<a href="#" class="img" style="background-image: url('images/plat (13).jpg');"></a>
    					<div class="text p-4">
    						<h3>Brochettes de boeuf</h3>
    						<p>Amusez vous en dégustant nos merveilleuses brochettes de viande !</p>
    						<p class="price"><span>€12,00</span> <a href="#" class="ml-2 btn btn-white btn-outline-white" onclick="commander('Brochettes de boeuf')">Commander</a></p>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<!-- <div class="container">
    		<div class="row justify-content-center mb-5 pb-3 mt-5 pt-5">
          <div class="col-md-7 heading-section text-center ftco-animate">
            <h2 class="mb-4">Our Menu Pricing</h2>
            <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
            <p class="mt-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-1.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Italian Pizza</span></h3>
	        				<span class="price">€20.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
        			</div>
        		</div>
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-2.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Hawaiian Pizza</span></h3>
	        				<span class="price">€29.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-3.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Greek Pizza</span></h3>
	        				<span class="price">€20.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-4.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Bacon Crispy Thins</span></h3>
	        				<span class="price">€20.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        	</div>

        	<div class="col-md-6">
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-5.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Hawaiian Special</span></h3>
	        				<span class="price">€49.91</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-6.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Ultimate Overload</span></h3>
	        				<span class="price">€20.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-7.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Bacon Pizza</span></h3>
	        				<span class="price">€20.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/pizza-8.jpg);"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>Ham &amp; Pineapple</span></h3>
	        				<span class="price">€20.00</span>
	        			</div>
	        			<div class="d-block">
	        				<p>A small river named Duden flows by their place and supplies</p>
	        			</div>
	        		</div>
        		</div>
        	</div>
        </div>
    	</div> -->
    </section>

<!-- Modal -->
<div class="modal fade" id="commanderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-dark" id="exampleModalScrollableTitle">Commander</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<form name="commanderForm" class="text-dark">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="names">Prénom & Nom</label>
						<input type="text" class="form-control" id="names" name="names" placeholder="Prénoms & noms" required>
					</div>
					<div class="form-group col-md-6">
						<label for="tel">Numéro de téléphone</label>
						<input type="tel" class="form-control" id="tel" name="tel" placeholder="Numéro de téléphone" required>
					</div>
				</div>
				<div class="form-group">
					<label for="adresse">Adresse</label>
					<input type="text" class="form-control" id="adresse" name="adresse" placeholder="Votre adresse" required>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="commande">Commande</label>
						<input type="text" class="form-control" id="commande" name="commande" placeholder="Nom de votre commande" required>
					</div>
					<div class="form-group col-md-6">
						<label for="quantite">Quantité</label>
						<input type="number" min="1" value="1" class="form-control" id="tel" name="quantite" placeholder="Quantité" required>
					</div>
				</div>
				<div class="form-group">
				<div class="form-check">
						<input class="form-check-input" type="radio" name="localisation" value="Je viens manger sur place" id="gridCheck" required>
						<label class="form-check-label" for="gridCheck">
							Je viens manger sur place
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="localisation" value="Je viens chercher moi même" id="gridCheck2" required>
						<label class="form-check-label" for="gridCheck2">
							Je viens chercher moi même
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="localisation" value="Je veux être livré" id="gridCheck3" required>
						<label class="form-check-label" for="gridCheck3">
							Je veux être livré
						</label>
					</div>
				</div>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="valider()">Valider</button>
      </div>
    </div>
  </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script>
	let produits = [];
	let i=0;
	
	produits.push({
		"nom": "Tilapia Braisé",
		"prix": 15,
		"description": "Vous aimez manger du poisson ? Vous allez adorer déguster ce plat !",
		"image": "plat (1).jpg"
	});
	produits.push({
		"nom": "Porc Braisé",
		"prix": 12,
		"description": "Vous avez des doutes ? Ce plat est notre spécialité, recommandé par 80% de nos clients",
		"image": "plat (2).jpg"
	});
	produits.push({
		"nom": "Poulet fermier entier braisé",
		"prix": 20,
		"description": "Mangez votre poulet à vous ! Braisé dans les règles de l'art culinaire camerounais. Vous allez l'apprécier",
		"image": "plat (7).jpg"
	});
	produits.push({
		"nom": "Eru",
		"prix": 13,
		"description": "Vous ne le connaissez pas ? Plat très prisé au Cameroun. Légumes cuits avec de l'huile de palme, contient des morceaux de viande.",
		"image": "plat (11).jpg"
	});
	produits.push({
		"nom": "Aile de poulet",
		"prix": 12,
		"description": "Nos ailes de poulets somptuesements cuisinés ! \" C'est la magie \" comme on dit au Cameroun",
		"image": "plat (5).jpg"
	});
	produits.push({
		"nom": "Capitaine",
		"prix": 20,
		"description": "Poisson très populaire et très apprécié. Venez le goûter avec nos assaisonnements délicats !",
		"image": "plat (10).jpg"
	});
		produits.push({
			"nom": "Ndolè",
			"prix": 13,
			"description": "L'un des plats les plus aimé au Cameroun. Notamment par les étrangers et les natifs, essayez et vous ne serez pas déçus !",
			"image": "plat (12).jpg"
	});

	produits.push({
			"nom": "Agneau braisé",
			"prix": 13,
			"description": "De la viande tendre d'agneau ?! Délicatement parfumé et cuisiné dans l'amour de nos traditions.",
			"image": "plat (3).jpg"
	});
	produits.push({
			"nom": "Brochettes de boeuf",
			"prix": 12,
			"description": "Regalez vous en dégustant nos merveilleuses brochettes de viande !",
			"image": "plat (13).jpg"
	});

	document.querySelector(".plates").innerHTML = "";
	produits.forEach(plat => {
		if(i<3 || i>5)
		document.querySelector(".plates").innerHTML += '<div class="col-lg-4 d-flex ftco-animate"><div class="services-wrap d-flex"><a href="#" class="img" style="background-image: url(\'images/'+plat.image+'\');"></a><div class="text p-4"><h3>'+plat.nom+'</h3><p>'+plat.description+'</p><p class="price"><span>€'+plat.prix+',00</span> <button class="ml-2 btn btn-white btn-outline-white" onclick="commander('+i+')">Commander</button></p></div></div></div>';
		else
		document.querySelector(".plates").innerHTML += '<div class="col-lg-4 d-flex ftco-animate"><div class="services-wrap d-flex"><a href="#" class="img order-lg-last" style="background-image: url(\'images/'+plat.image+'\');"></a><div class="text p-4"><h3>'+plat.nom+'</h3><p>'+plat.description+'</p><p class="price"><span>€'+plat.prix+',00</span> <button class="ml-2 btn btn-white btn-outline-white" onclick="commander('+i+')">Commander</button></p></div></div></div>';
		i++;
	});

	var produit

	function commander(id){
		$('#commanderModal').modal('toggle');
		document.commanderForm.commande.value = produits[id].nom;
		produit = produits[id]
	}
	
	function valider(){
		let noms = document.commanderForm.names.value;
		let tels = document.commanderForm.tel.value;
		let adresse = document.commanderForm.adresse.value;
		let commande = document.commanderForm.commande.value;
		let quantite = document.commanderForm.quantite.value;
		let localisation = document.commanderForm.localisation.value;
		let mess = "https://api.whatsapp.com/send?phone=33782789689&text="+encodeURI("Salut Monsieur, Je suis "+noms+". Je suis à l'adresse : "+adresse+" et je souhaiterai avoir "+quantite+" plat(s) de "+commande+". "+localisation+". Je suis disponible ai numéro "+tels);
		// window.open(mess, "_blank"); 
		window.open("pages/payment.php?tel="+tels+"&produit="+produit.nom+"&description="+produit.description+"&image="+produit.image+"&prix="+produit.prix+"&qte="+quantite); 
	}
</script>