<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Bar Restaurant L'Ambassade - Rouen</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="shortcut icon" href="images/plat%20(13).jpg" type="image/x-icon">
    <meta property="og:image" content="https://ambassade-rouen.fr/images/plat%20(13).jpg" />
    <meta property="og:type" content="Food Service" />
    <meta property="og:title" content="Votre meilleure cuisine africaine de Rouen chez l'Ambassade" />
    <meta property="og:description" content="Réservez votre commande en ligne et retrouvez toutes les délices de la cuisine camerounaise au Bar restaurant l'ambassade" />
    <meta property="og:url" content="https://ambassade-rouen.fr/" />
    <meta property="og:site_name" content="Votre Restaurant camerounais de Rouen : L'Ambassade" />
   
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <style>
      form .form-control{
        color: #333!important;
      }
    </style>
  </head>
  <body>
			<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
					<div class="container">
							<a class="navbar-brand" href="">L'Ambassade<br><small>Bar-Restaurant</small></a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
								<span class="oi oi-menu"></span> Menu
							</button>
						<div class="collapse navbar-collapse" id="ftco-nav">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item active"><a href="index.php" class="nav-link">Accueil</a></li>
								<li class="nav-item"><a href="services.php" class="nav-link">Services</a></li>
								<li class="nav-item"><a href="menu.php" class="nav-link">Menu</a></li>
								<li class="nav-item"><a href="index.php#gallery" class="nav-link">Gallery</a></li>
								<li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
								<li class="nav-item"><a href="about.php" class="nav-link">À propos</a></li>
							</ul>
						</div>
					</div>
				</nav>
    <!-- END nav -->

   <?php include "pages/accueil.php"; ?>
   <?php include "pages/intro.php"; ?>
   <?php include "pages/about.php"; ?>
   <?php include "pages/services.php"; ?>
   <?php include "pages/menu.php"; ?>
   <?php include "pages/gallery.php"; ?>
   <?php include "pages/counter.php"; ?>
   <?php include "pages/menu2.php"; ?>
   <?php include "pages/contact.php"; ?>
   <?php include "pages/footer.php"; ?>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
  
  <script>
    
		if (!document.location.href.includes("https") && (!document.location.href.includes("localhost") && !document.location.href.includes("htdocs"))) {
			document.location.href = "https://ambassade-rouen.fr";
		}
  </script>
  </body>
</html>